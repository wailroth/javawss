package fr.wailroth.walesound.utils;

/*
This code is owned by Alexis Dumain, aliase wailroth.
This file was created the 24/06/2021.
Ask me to use this code, at wailroth#4736.
**/


import fr.wailroth.walesound.WaleSound;
import fr.wailroth.walesound.area.WLocation;
import org.bukkit.Location;

public class LocationUtil {

    /**
     * Check if player in inside location
     *
     * @param playerLocation player location
     * @param l1             location 1
     * @param l2             location 2
     * @return if player is inside
     */
    public static boolean isInside(Location playerLocation, Location l1, Location l2) {
        int x1 = Math.min(l1.getBlockX(), l2.getBlockX());
        int z1 = Math.min(l1.getBlockZ(), l2.getBlockZ());
        int x2 = Math.max(l1.getBlockX(), l2.getBlockX());
        int z2 = Math.max(l1.getBlockZ(), l2.getBlockZ());

        return playerLocation.getX() >= x1
                && playerLocation.getX() <= x2
                && playerLocation.getZ() >= z1
                && playerLocation.getZ() <= z2;
    }

    /**
     * Get wLocation by his name
     *
     * @param locationName sound name
     * @return wLocation
     */
    public static WLocation getLocationByName(String locationName) {

        for (WLocation wLocation : WaleSound.getWaleSound().getwLocationList()) {
            if (!wLocation.getName().equals(locationName)) continue;

            return wLocation;
        }
        return null;
    }

}


