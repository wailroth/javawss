package fr.wailroth.walesound.commands;

/*
This code is owned by Alexis Dumain, aliase wailroth.
This file was created the 24/06/2021.
Ask me to use this code, at wailroth#4736.
**/


import fr.wailroth.walesound.WaleSound;
import fr.wailroth.walesound.commandfw.CommandArgs;
import fr.wailroth.walesound.rabbitmq.RabbitManager;
import fr.wailroth.walesound.rabbitmq.RabbitMq;
import fr.wailroth.walesound.utils.LocationUtil;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.entity.Player;

public class WaleCommand {

    @fr.wailroth.walesound.commandfw.Command(name = "wale",
            description = "This command help you to know how Wale work.",
            permission = "wale.help",
            usage = "/wale")

    public void wale(CommandArgs args) {
        if (!args.isPlayer()) {
            args.getSender().sendMessage("You need to be in-game");
            return;
        }

        Player player = args.getPlayer();
        sendHelp(player);
    }

    @fr.wailroth.walesound.commandfw.Command(name = "wale.create",
            description = "This command will create area.",
            permission = "wale.create",
            usage = "/wale create")
    public void waleCreate(CommandArgs args) {
        if (!args.isPlayer()) {
            args.getSender().sendMessage("You need to be in-game");
            return;
        }
        Player player = args.getPlayer();

        WaleSound.getWaleSound().getCreatingClaim().add(player.getUniqueId());

        TextComponent textComponent = new TextComponent();
        textComponent.setText("§aClick §b§lhere §afor set the position 1");
        textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                new ComponentBuilder("§aClick here").create()));
        textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/wale pos1"));

        player.spigot().sendMessage(textComponent);
    }

    @fr.wailroth.walesound.commandfw.Command(name = "wale.pos1",
            description = "This command will set pos1",
            permission = "wale.create",
            usage = "/wale pos1")
    public void walePos1(CommandArgs args) {

        if (!args.isPlayer()) {
            args.getSender().sendMessage("You need to be in-game");
            return;
        }


        Player player = args.getPlayer();

        if (!WaleSound.getWaleSound().getCreatingClaim().contains(player.getUniqueId())) {
            player.sendMessage("§cUse /wale create to create new claim.");
            return;
        }

        if (WaleSound.getWaleSound().getCommandLocation1().get(player.getUniqueId()) != null) {
            TextComponent textComponent = new TextComponent("§cYou can't redefine location." +
                    " §4§lType 'cancel' or click here");
            textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "cancel"));
            textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                    new ComponentBuilder("§aClick here").create()));
            player.spigot().sendMessage(textComponent);
            return;
        }

        TextComponent textComponent = new TextComponent("§aClick §b§lhere §afor set the position 2");
        textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/wale pos2"));
        textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                new ComponentBuilder("§aClick here").create()));
        WaleSound.getWaleSound().getCommandLocation1().put(player.getUniqueId(), player.getLocation());
        player.spigot().sendMessage(textComponent);

    }

    @fr.wailroth.walesound.commandfw.Command(name = "wale.pos2",
            description = "This command will set pos2",
            permission = "wale.create",
            usage = "/wale pos2")
    public void walePos2(CommandArgs args) {
        if (!args.isPlayer()) {
            args.getSender().sendMessage("You need to be in-game");
            return;
        }
        Player player = args.getPlayer();

        if (!WaleSound.getWaleSound().getCreatingClaim().contains(player.getUniqueId())) {
            player.sendMessage("§cUse /wale create to create new claim.");
            return;
        }


        if (WaleSound.getWaleSound().getCommandLocation2().get(player.getUniqueId()) != null) {
            TextComponent textComponent = new TextComponent("§cYou can't redefine location." +
                    " §4§lType 'cancel' or click here");
            textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "cancel"));
            textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                    new ComponentBuilder("§aClick here").create()));
            player.spigot().sendMessage(textComponent);
            return;
        }

        WaleSound.getWaleSound().getCommandLocation2().put(player.getUniqueId(), player.getLocation());
        player.sendMessage("§aPlease enter location name. §c§lType cancel to cancel.");
    }

    @fr.wailroth.walesound.commandfw.Command(name = "wale.hear",
            description = "This command will allow you to hear if you can't !",
            usage = "/wale hear")
    public void waleHear(CommandArgs args) {
        if (!args.isPlayer()) {
            args.getSender().sendMessage("You need to be in-game");
            return;
        }
        Player player = args.getPlayer();

        player.sendMessage("§cNote: click on the button on the site to activate the sound. " +
                "If you hear twice, refresh page.");

        RabbitMq.sendSoundPacket(player.getName(), WaleSound.getWaleSound().getConfiguration().getDefaultSoundName(),
                RabbitManager.getBasicChannel());

    }


    @fr.wailroth.walesound.commandfw.Command(name = "wale.link",
            description = "This command will give you website link.",
            usage = "/wale link")
    public void waleLink(CommandArgs args) {
        if (!args.isPlayer()) {
            args.getSender().sendMessage("You need to be in-game");
            return;
        }
        Player player = args.getPlayer();

        TextComponent textComponent = new TextComponent(WaleSound.getWaleSound().getConfiguration().getJoinMessage());
        textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                new ComponentBuilder("§aClick me !").create()));
        textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL,
                WaleSound.getWaleSound().getConfiguration().getWebLink(player)));

    }

    @fr.wailroth.walesound.commandfw.Command(name = "wale.delete",
            description = "This command will delete claim.",
            permission = "wale.delete",
            usage = "/wale delete locationName")
    public void waleDelete(CommandArgs args) {
        if (!args.isPlayer()) {
            args.getSender().sendMessage("You need to be in-game");
            return;
        }
        Player player = args.getPlayer();

        String name = args.getArgs(0);

        if (name == null) {
            player.sendMessage("/wale delete locationName");
            return;
        }


        if (LocationUtil.getLocationByName(name) == null) {
            player.sendMessage("§cThe location is invalid.");
            return;
        }

        WaleSound.getWaleSound().getwLocationList().remove(LocationUtil.getLocationByName(name));
        player.sendMessage("§aYou successfully deleted " + name);
    }


    public void sendHelp(Player player) {
        player.sendMessage("§1§m----- &r§bWSS §1§m-----");
        player.sendMessage("§5/wale create : §a Allows you to create area.");
        player.sendMessage("§5/wale hear : §a Debug sound.");
        player.sendMessage("§5/wale link : §a Give your sound link.");
        player.sendMessage("§5/wale remove <SoundName> : §a Remove location with sound name.");
    }
}


