package fr.wailroth.walesound.license;

/*
This code is owned by Alexis Dumain, aliase wailroth.
This file was created the 26/06/2021.
Ask me to use this code, at wailroth#4736.
**/


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class License {


    private final String owner;
    private final String license;


    /**
     * License object
     */
    public License() {
        owner = "";
        license = "";
    }

    /**
     * Check if license is ok
     *
     * @return boolean
     */
    public boolean checkActiveLicense() {


        if (owner.isEmpty() || license.isEmpty()) {
            return false;
        }

        String content = "";
        try {
            URL url = new URL("https://wailroth.com/licenses/license.php?owner=" + owner + "&license=" + license);
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            String str;
            while ((str = in.readLine()) != null) {
                content += str;
            }
            in.close();
        } catch (IOException e) {
        }


        switch (content) {
            case "":
            case "true":
                return true;
            default:
                return false;
        }
    }

    /**
     * Get license
     *
     * @return license
     */
    public String getLicense() {
        return license;
    }

    /**
     * Get owner
     *
     * @return owner
     */
    public String getOwner() {
        return owner;
    }
}



