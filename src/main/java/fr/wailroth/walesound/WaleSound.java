package fr.wailroth.walesound;

/*
This code is owned by Alexis Dumain, aliase wailroth.
This file was created the 23/06/2021.
Ask me to use this code, at wailroth#4736.
**/


import com.google.gson.reflect.TypeToken;
import fr.wailroth.walesound.area.WLocation;
import fr.wailroth.walesound.commandfw.CommandFramework;
import fr.wailroth.walesound.commands.WaleCommand;
import fr.wailroth.walesound.configuration.Configuration;
import fr.wailroth.walesound.json.JsonManager;
import fr.wailroth.walesound.license.License;
import fr.wailroth.walesound.listeners.PlayerListener;
import fr.wailroth.walesound.rabbitmq.RabbitManager;
import fr.wailroth.walesound.rabbitmq.RabbitMq;
import fr.wailroth.walesound.utils.CooldownAPI;
import fr.wailroth.walesound.utils.FileUtil;
import fr.wailroth.walesound.utils.LocationUtil;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class WaleSound extends JavaPlugin {

    private File configurationFile;
    private File locationFile;
    private File licenseFile;

    private Configuration configuration;
    private JsonManager jsonManager;

    private List<WLocation> wLocationList;

    private Map<UUID, String> playingSound;

    private static WaleSound waleSound;

    private HashMap<UUID, Location> commandLocation1 = new HashMap<>();
    private HashMap<UUID, Location> commandLocation2 = new HashMap<>();
    private HashMap<UUID, String> locationName = new HashMap<>();

    private HashMap<UUID, WLocation> actualZone = new HashMap<>();

    private List<UUID> creatingClaim = new ArrayList<>();

    private CommandFramework commandFramework;

    private License license;

    //
    private int licenseTry = 0;
    private int licenseSuccess = 0;


    /**
     * When plugin is enable
     */
    @Override
    public void onEnable() {
        waleSound = this;
        this.wLocationList = new ArrayList<>();
        this.jsonManager = new JsonManager();
        this.playingSound = new HashMap<>();

        //Register license
        registerLicense();

        //Register listeners
        registerListeners();

        //Register commands
        registerCommands();

        //Config file
        registerConfigFile();

        //RabbitMQ
        RabbitManager.createConnection(configuration.getRabbitHost());
        RabbitManager.createChannels();
        RabbitManager.declareQueue();


        registerLocationFile();
        registerCooldowns();
        saveTask();
        playSound();


    }


    /**
     * When plugin is disable
     */
    @Override
    public void onDisable() {
        saveLocationFile();
    }


    /**
     * Register listeners
     */
    public void registerListeners() {
        PluginManager pluginManager = Bukkit.getPluginManager();

        pluginManager.registerEvents(new PlayerListener(), this);
    }

    /**
     * Register commands
     */
    public void registerCommands() {
        commandFramework = new CommandFramework(this);
        commandFramework.registerCommands(new WaleCommand());
    }

    /**
     * Register configuration file
     */
    public void registerConfigFile() {
        configurationFile = new File(getDataFolder() + "/config.json");
        if (!configurationFile.exists()) {
            try {
                FileUtil.save(configurationFile, jsonManager.serialize(new Configuration()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        try {
            configuration = jsonManager.deserialize(FileUtil.loadContent(configurationFile), Configuration.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Register location file
     * This file store all claims, with sound name.
     */
    public void registerLocationFile() {
        this.locationFile = new File(getDataFolder() + "/location.json");
        if (!locationFile.exists()) {
            try {
                FileUtil.createFile(locationFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                TypeToken<List<WLocation>> typeToken = new TypeToken<List<WLocation>>() {
                };
                wLocationList.addAll(jsonManager.deserializeList(FileUtil.loadContent(locationFile), typeToken));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private void registerLicense() {
        licenseFile = new File(getDataFolder() + "/license.json");
        if (!licenseFile.exists()) {
            try {
                FileUtil.save(licenseFile, jsonManager.serialize(new License()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        try {
            license = jsonManager.deserialize(FileUtil.loadContent(licenseFile), License.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        while (licenseTry < 3) {
            licenseTry++;
            if (license.checkActiveLicense()) {
                licenseSuccess++;
            }
        }

        if (licenseSuccess == 0) {
            Bukkit.getLogger().info("[WSS] Your license is invalid or the website is down." +
                    " Contact wailroth on discord");
            Bukkit.shutdown();
        }

    }

    /**
     * Save location file
     */
    public void saveLocationFile() {

        String serialize = jsonManager.serializeList(wLocationList);

        try {
            FileUtil.save(locationFile, serialize);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This task save location file
     */
    public void saveTask() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, this::saveLocationFile, 0, 20 * 10);
    }

    public void playSound() {
        Bukkit.getScheduler().scheduleAsyncRepeatingTask(this, () -> {
            for (Player player : Bukkit.getOnlinePlayers()) {
                for (WLocation wLocation : wLocationList) {
                    if (LocationUtil.isInside(player.getLocation(), wLocation.getLoc1(), wLocation.getLoc2())) {
                        actualZone.put(player.getUniqueId(), wLocation);
                    } else {
                        actualZone.put(player.getUniqueId(), null);
                    }
                }

                if (!CooldownAPI.isOnCooldown("change", player)) {
                    sendSoundToPlayer(player);
                }


            }
        }, 0, 10);

    }


    /**
     * Send sound to player
     *
     * @param player the player
     */
    public void sendSoundToPlayer(Player player) {
        WLocation wLocation = actualZone.get(player.getUniqueId());
        if (wLocation == null) {
            if (configuration.isDefaultSoundEnable()) {
                if (!playingSound.get(player.getUniqueId()).equals(configuration.getDefaultSoundName())) {
                    playingSound.put(player.getUniqueId(), configuration.getDefaultSoundName());
                    RabbitMq.sendSoundPacket(player.getName(), configuration.getDefaultSoundName(),
                            RabbitManager.getBasicChannel());
                    player.sendMessage("§a§lWSS: §fChanged to §e" + configuration.getDefaultSoundName());
                    CooldownAPI.addCooldown("change", player, 2);
                }
            } else {
                RabbitMq.sendSoundPacket(player.getName(), "stop", RabbitManager.getBasicChannel());
                playingSound.put(player.getUniqueId(), null);
            }

        } else {
            if (!playingSound.get(player.getUniqueId()).equals(wLocation.getSound())) {
                playingSound.put(player.getUniqueId(), wLocation.getSound());
                RabbitMq.sendSoundPacket(player.getName(), wLocation.getSound(), RabbitManager.getBasicChannel());
                player.sendMessage("§a§lWSS: §fChanged to §e" + wLocation.getName());
                CooldownAPI.addCooldown("change", player, 2);
            }

        }

    }


    public void registerCooldowns() {
        CooldownAPI.createCooldown("change", 2);
    }

    /**
     * Get configuration
     *
     * @return configuration
     */
    public Configuration getConfiguration() {
        return configuration;
    }

    /**
     * Get json manager
     *
     * @return json manager
     */
    public JsonManager getJsonManager() {
        return jsonManager;
    }

    /**
     * Get playing sound
     *
     * @return playing sound
     */
    public Map<UUID, String> getPlayingSound() {
        return playingSound;
    }

    public static WaleSound getWaleSound() {
        return waleSound;
    }

    /**
     * Get command location 1
     *
     * @return location
     */
    public HashMap<UUID, Location> getCommandLocation1() {
        return commandLocation1;
    }

    /**
     * Get command location 2
     *
     * @return location
     */
    public HashMap<UUID, Location> getCommandLocation2() {
        return commandLocation2;
    }

    /**
     * Get player creating claim
     *
     * @return creating claim
     */
    public List<UUID> getCreatingClaim() {
        return creatingClaim;
    }


    /**
     * Get W Location list
     *
     * @return W Location list
     */
    public List<WLocation> getwLocationList() {
        return wLocationList;
    }

    /**
     * Get actual zone
     *
     * @return actual zone
     */
    public HashMap<UUID, WLocation> getActualZone() {
        return actualZone;
    }

    /**
     * Get license file
     *
     * @return license file
     */
    public File getLicenseFile() {
        return licenseFile;
    }

    /**
     * Get license
     *
     * @return license
     */
    public License getLicense() {
        return license;
    }

    /**
     * Get location name
     *
     * @return location name
     */
    public HashMap<UUID, String> getLocationName() {
        return locationName;
    }
}


