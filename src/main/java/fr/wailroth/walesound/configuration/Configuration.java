package fr.wailroth.walesound.configuration;

/*
This code is owned by Alexis Dumain, aliase wailroth.
This file was created the 23/06/2021.
Ask me to use this code, at wailroth#4736.
**/


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Configuration {


    private final String rabbitHost;
    private List<String> sounds;
    private final String commandPermission;
    private final boolean isDefaultSoundEnable;
    private final String defaultSoundName;
    private final String joinMessage;
    private final String webLink;
    private final String title;
    private final String subTitle;

    /*
     * Configuration object
     */
    public Configuration() {
        this.sounds = new ArrayList<>();
        this.rabbitHost = "localhost";
        this.commandPermission = "wale.admin";
        sounds.addAll(Arrays.asList("Nether.mp3", "Sound.wav"));
        isDefaultSoundEnable = true;
        defaultSoundName = "default.mp3";
        joinMessage = "&aPlease click on this link to hear new sounds! §c§lDon't forget to click on the green button !";
        webLink = "http://localhost/user/<player>";
        title = "&aWSS";
        subTitle = "&a&lPlease, click the link in the chat !";
    }


    /**
     * Get rabbit host
     *
     * @return rabbit host
     */
    public String getRabbitHost() {
        return rabbitHost;
    }

    /**
     * Get sounds
     *
     * @return sounds
     */
    public List<String> getSounds() {
        return sounds;
    }

    /**
     * Get command permission
     *
     * @return command permission
     */
    public String getCommandPermission() {
        return commandPermission;
    }

    /**
     * Get default sound name
     *
     * @return default sound name
     */
    public String getDefaultSoundName() {
        return defaultSoundName;
    }

    /**
     * Is default sound name is enable
     *
     * @return is default sound name enable
     */
    public boolean isDefaultSoundEnable() {
        return isDefaultSoundEnable;
    }

    public String getJoinMessage() {
        return ChatColor.translateAlternateColorCodes('&',  joinMessage);
    }

    /**
     * Get web link
     *
     * @param player player
     * @return webLink
     */
    public String getWebLink(Player player) {
        return webLink.replace("<player>", player.getName());
    }

    /**
     * Get title
     *
     * @return title
     */
    public String getTitle() {
        return ChatColor.translateAlternateColorCodes('&', title);
    }

    /**
     * Get subtitle
     *
     * @return subtitle
     */
    public String getSubTitle() {
        return ChatColor.translateAlternateColorCodes('&', subTitle);
    }
}



