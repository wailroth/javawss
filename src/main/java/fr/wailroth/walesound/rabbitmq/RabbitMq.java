package fr.wailroth.walesound.rabbitmq;

/*
This code is owned by Alexis Dumain, aliase wailroth.
This file was created the 23/06/2021.
Ask me to use this code, at wailroth#4736.
**/


import com.rabbitmq.client.Channel;
import com.rabbitmq.client.MessageProperties;
import java.io.IOException;
import org.bukkit.Bukkit;

public class RabbitMq {


    public static void sendSoundPacket(String playerName, String sound, Channel channel) {
        try {
            channel.basicPublish("", "SoundQueue",
                    MessageProperties.PERSISTENT_TEXT_PLAIN,
                    (playerName + "@" + sound).getBytes("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Bukkit.getLogger().info("Player: " + playerName + " request for " + sound);
    }


    public static void sendConnect(String playerName, Channel channel) {
        try {
            channel.basicPublish("", "ConnectQueue",
                    MessageProperties.PERSISTENT_TEXT_PLAIN,
                    playerName.getBytes("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void sendDisconnect(String playerName, Channel channel) {
        try {
            channel.basicPublish("", "DisconnectQueue",
                    MessageProperties.PERSISTENT_TEXT_PLAIN,
                    playerName.getBytes("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}


