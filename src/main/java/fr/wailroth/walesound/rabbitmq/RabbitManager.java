package fr.wailroth.walesound.rabbitmq;

/*
This code is owned by Alexis Dumain, aliase wailroth.
This file was created the 25/06/2021.
Ask me to use this code, at wailroth#4736.
**/


import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RabbitManager {

    private static Connection rabbitConnection;
    private static Channel basicChannel;


    /**
     * Create rabbit connection
     *
     * @param host rabbit host
     */
    public static void createConnection(String host) {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        try {
            Connection connection = factory.newConnection();
            rabbitConnection = connection;


        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create rabbit channel
     */
    public static void createChannels() {
        try {
            Channel channel = rabbitConnection.createChannel();
            basicChannel = channel;
        } catch (IOException e) {
            e.printStackTrace();
        }
        {

        }
    }

    /**
     * Declare queues
     */
    public static void declareQueue() {
        try {
            basicChannel.queueDeclare("SoundQueue",
                    true,
                    false,
                    false,
                    null);

            basicChannel.queueDeclare("ConnectQueue",
                    true,
                    false,
                    false,
                    null);

            basicChannel.queueDeclare("DisconnectQueue",
                    true,
                    false,
                    false,
                    null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get rabbit connection
     *
     * @return rabbit connection
     */
    public static Connection getRabbitConnection() {
        return rabbitConnection;
    }

    /**
     * Get basic channel
     *
     * @return basic channel
     */
    public static Channel getBasicChannel() {
        return basicChannel;
    }
}


