package fr.wailroth.walesound.listeners;

/*
This code is owned by Alexis Dumain, aliase wailroth.
This file was created the 23/06/2021.
Ask me to use this code, at wailroth#4736.
**/


import fr.wailroth.walesound.WaleSound;
import fr.wailroth.walesound.area.WLocation;
import fr.wailroth.walesound.rabbitmq.RabbitManager;
import fr.wailroth.walesound.rabbitmq.RabbitMq;
import fr.wailroth.walesound.utils.LocationUtil;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onJoin(PlayerJoinEvent event) {
        event.setJoinMessage(null);
        Player player = event.getPlayer();

        TextComponent textComponent = new TextComponent(WaleSound.getWaleSound().getConfiguration().getJoinMessage());
        textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                new ComponentBuilder("§aClick me !").create()));
        textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL,
                WaleSound.getWaleSound().getConfiguration().getWebLink(player)));

        player.spigot().sendMessage(textComponent);

        RabbitMq.sendConnect(player.getName(), RabbitManager.getBasicChannel());
        WaleSound.getWaleSound().getPlayingSound().put(player.getUniqueId(), "null");

        player.sendTitle(WaleSound.getWaleSound().getConfiguration().getTitle(),
                WaleSound.getWaleSound().getConfiguration().getSubTitle());

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        RabbitMq.sendDisconnect(player.getName(), RabbitManager.getBasicChannel());
        WaleSound.getWaleSound().getPlayingSound().remove(player.getUniqueId());
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onChat(AsyncPlayerChatEvent event) {

        if (!WaleSound.getWaleSound().getCreatingClaim().contains(event.getPlayer().getUniqueId())) {
            return;
        }
        Player player = event.getPlayer();

        if (event.getMessage().equalsIgnoreCase("cancel")) {
            removePlayer(player);
            player.sendMessage("§cCanceled");
            event.setCancelled(true);
            return;
        }

        if (WaleSound.getWaleSound().getLocationName().get(player.getUniqueId()) == null) {
            event.setCancelled(true);

            if(LocationUtil.getLocationByName(event.getMessage()) != null) {
                player.sendMessage("§cChoose an other name.");
                return;
            }

            WaleSound.getWaleSound().getLocationName().put(player.getUniqueId(), event.getMessage());
            player.sendMessage("§aNow, enter the name of the song." +
                    " §4§lPLEASE, WITH .mp3, or .wav ! Type cancel to cancel");
            return;

        }

        Location loc1 = WaleSound.getWaleSound().getCommandLocation1().get(player.getUniqueId());
        Location loc2 = WaleSound.getWaleSound().getCommandLocation2().get(player.getUniqueId());

        WaleSound.getWaleSound().getwLocationList().add(new WLocation(
                WaleSound.getWaleSound().getLocationName().get(player.getUniqueId()),
                event.getMessage(),
                event.getPlayer().getUniqueId(),
                (int) loc1.getX(),
                0,
                (int) loc1.getZ(),
                (int) loc2.getX(),
                255,
                (int) loc2.getZ(),
                loc2.getWorld().getName()));

        player.sendMessage("§aSuccess !");
        removePlayer(player);

        event.setCancelled(true);
    }


    public void removePlayer(Player player) {
        WaleSound.getWaleSound().getCommandLocation1().remove(player.getUniqueId());
        WaleSound.getWaleSound().getCommandLocation1().remove(player.getUniqueId());
        WaleSound.getWaleSound().getCreatingClaim().remove(player.getUniqueId());
        WaleSound.getWaleSound().getLocationName().remove(player.getUniqueId());
    }


}


