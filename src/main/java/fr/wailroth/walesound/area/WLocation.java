package fr.wailroth.walesound.area;

/*
This code is owned by Alexis Dumain, aliase wailroth.
This file was created the 23/06/2021.
Ask me to use this code, at wailroth#4736.
**/


import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.Location;

public class WLocation {


    private String name;
    private String sound;
    private UUID claimedBy;
    private double pos1X;
    private double pos1Y;
    private double pos1Z;
    private double pos2X;
    private double pos2Y;
    private double pos2Z;
    private String world;

    /**
     * Wale location object
     *
     * @param name location name
     * @param claimedBy claimed by
     * @param sound sound
     * @param pos1X position 1 X
     * @param pos1Y position 1 Y
     * @param pos1Z position 1 Z
     * @param pos2X position 2 Z
     * @param pos2Y position 2 Z
     * @param pos2Z position 2 Z
     * @param world world
     */
    public WLocation(String name,
                     String sound,
                     UUID claimedBy,
                     double pos1X,
                     double pos1Y,
                     double pos1Z,
                     double pos2X,
                     double pos2Y,
                     double pos2Z,
                     String world) {
        this.name = name;
        this.sound = sound;
        this.claimedBy = claimedBy;
        this.pos1X = pos1X;
        this.pos1Y = pos1Y;
        this.pos1Z = pos1Z;
        this.pos2X = pos2X;
        this.pos2Y = pos2Y;
        this.pos2Z = pos2Z;
        this.world = world;
    }

    /**
     * Get pos 2Z
     *
     * @return pos1X
     */
    public double getPos1X() {
        return pos1X;
    }

    /**
     * Get pos 2Z
     *
     * @return pos1Y
     */
    public double getPos1Y() {
        return pos1Y;
    }

    /**
     * Get pos 2Z
     *
     * @return pos1Z
     */
    public double getPos1Z() {
        return pos1Z;
    }

    /**
     * Get pos 2Z
     *
     * @return pos2X
     */
    public double getPos2X() {
        return pos2X;
    }

    /**
     * Get pos 2Z
     *
     * @return pos2Y
     */
    public double getPos2Y() {
        return pos2Y;
    }

    /**
     * Get pos 2Z
     *
     * @return pos2Z
     */
    public double getPos2Z() {
        return pos2Z;
    }

    /**
     * Get world
     *
     * @return world
     */
    public String getWorld() {
        return world;
    }

    /**
     * Get location 1
     *
     * @return location 1
     */
    public Location getLoc1() {
        return new Location(Bukkit.getWorld(world), pos1X, pos1Y, pos1Z);
    }

    /**
     * Get location 2
     *
     * @return location 2
     */
    public Location getLoc2() {
        return new Location(Bukkit.getWorld(world), pos2X, pos2Y, pos2Z);
    }

    /**
     * Get who claimed area
     *
     * @return who claimed
     */
    public UUID getClaimedBy() {
        return claimedBy;
    }

    /**
     * Get sound
     *
     * @return sound
     */
    public String getSound() {
        return sound;
    }

    /**
     * Get location name
     *
     * @return name
     */
    public String getName() {
        return name;
    }
}


