package fr.wailroth.walesound.json;

/*
This code is owned by Alexis Dumain, aliase wailroth.
This file was created the 23/06/2021.
Ask me to use this code, at wailroth#4736.
**/


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.util.List;

public class JsonManager {


    private final Gson gson;


    /**
     * Constructor
     */
    public JsonManager() {
        this.gson = createGsonInstance();
    }


    /**
     * Create gson instance
     *
     * @return Gson
     */

    private Gson createGsonInstance() {
        return new GsonBuilder()
                .setPrettyPrinting()
                .serializeNulls()
                .disableHtmlEscaping()
                .create();
    }


    /**
     * Serialize object
     *
     * @param object classe
     * @return String json
     */
    public String serialize(Object object) {
        return this.gson.toJson(object);
    }

    /**
     * Deserialize object
     *
     * @param json json
     * @param type class
     * @return Object class
     */
    public <T> T deserialize(String json, Class<T> type) {
        return this.gson.fromJson(json, type);
    }

    /**
     * Deserialize list
     *
     * @param json json
     * @param typeToken class
     * @return list of class
     */
    public <T> List<T> deserializeList(String json, TypeToken<List<T>> typeToken) {
        return this.gson.fromJson(json, typeToken.getType());
    }

    /**
     * Serialize list
     *
     * @param objectList class
     * @return JSON string
     */
    public <T> String serializeList(List<T> objectList) {
        return this.gson.toJson(objectList);
    }




}


